<?xml version="1.0" encoding="UTF-8"?>
<data>
  <struct name="USUM Main">
    <array name="unknown" type="uint8" length="0x1400" />

    <struct name="Player Info">
      <primitive name="Trainer ID" type="uint16" />
      <primitive name="Secret ID" type="uint16" />
      <enum name="Game" enum="PlayerInfo.Game" type="uint8" />
      <enum name="Gender" enum="PlayerInfo.Gender" type="uint8" />

      <array name="unknown" type="uint8" length="40" />

      <primitive name="3DS Sub-Region (dependent on country)" type="uint8" />
      <enum name="3DS Country" enum="PlayerInfo.Country" type="uint8" />

      <array name="unknown" type="uint8" length="4" />

      <enum name="3DS Region" enum="PlayerInfo.3dsRegion" type="uint8" />
      <enum name="3DS Language" enum="PlayerInfo.Language" type="uint8" />

      <array name="padding" type="uint8" length="2" />

      <array name="Player Name (UTF-16)" type="uint8" length="24" />

      <array name="padding" type="uint8" length="4" />

      <enum name="Skin Tint" enum="PlayerInfo.SkinTint" type="uint8" />

      <array name="padding" type="uint8" length="3" />

      <primitive name="Multiplayer Sprite ID (unused)" type="uint8" />

      <array name="padding" type="uint8" length="1" />
      <array name="unknown" type="uint8" length="0x1E" />

      <flags name="Special Move Unlocks" enum="PlayerInfo.SpecialMoveUnlocks" type="uint8" />

      <array name="unknown" type="uint8" length="1" />

      <enum name="Ball Throw Style" enum="PlayerInfo.BallThrowStyle" type="uint8" />

      <array name="padding" type="uint8" length="0x185" />

    </struct>

    <array name="unknown" type="uint8" length="0x3600" />

    <struct name="PC / Box Layout">
      <!--
        String length is always identified by "terminatedBy" by Okteka.
        However, the names have a fixed reserved space of 34 bytes. This does not seem supported by Okteka.
      -->
      <array name="Box Names" length="32">
        <!-- <string name="Name" encoding="UTF-16" maxBytecount="34" maxCharCount="17" length="34"/> -->
        <array name="Name (UTF-16)" type="uint8" length="34" />
      </array>
      <array name="Team Names" length="6">
        <!-- <string name="Name" encoding="UTF-16" maxBytecount="22" maxCharCount="11" length="22"/> -->
        <array name="Name (UTF-16)" type="uint8" length="22" />
      </array>

      <array name="unknown" type="uint8" length="0x48" />
      <array name="unknown" type="uint8" length="0xB4" />

      <array name="Box Backgrounds" length="32">
        <enum name="Background" enum="PcBoxBackground" type="uint8" />
      </array>

      <flags name="Pc Box Flags" enum="PcBoxFlags" type="uint8" />
      <primitive name="Unlocked Count" type="uint8" />

      <array name="padding" type="uint8" length="30" />
    </struct>
  </struct>

  <!-- Enum Definitions -->

  <!-- Player Info -->

  <enumDef name="PlayerInfo.Game" type="uint8">
    <entry value="30" name="Sun" />
    <entry value="31" name="Moon" />
    <entry value="32" name="Ultra sun" />
    <entry value="33" name="Ultra Moon" />
  </enumDef>

  <enumDef name="PlayerInfo.Gender" type="uint8">
    <entry value="0" name="Male" />
    <entry value="1" name="Female" />
  </enumDef>

  <!-- TODO: Complete this enum -->
  <enumDef name="PlayerInfo.Country" type="uint8">
    <entry value="0x40" name="Albania" />
    <entry value="0x4E" name="Germany" />
    <entry value="0x60" name="Norway" />
    <entry value="0x70" name="Zimbabwe" />
    <entry value="0x7A" name="Andorra" />
  </enumDef>

  <enumDef name="PlayerInfo.3dsRegion" type="uint8">
    <entry value="0x00" name="Japan" />
    <entry value="0x01" name="America" />
    <entry value="0x02" name="Europe" />
    <entry value="0x04" name="China" />
    <entry value="0x05" name="Korea" />
    <entry value="0x06" name="Taiwan" />
  </enumDef>

  <enumDef name="PlayerInfo.Language" type="uint8">
    <entry value="0x01" name="Japanese" />
    <entry value="0x02" name="English" />
    <entry value="0x03" name="French" />
    <entry value="0x04" name="Italian" />
    <entry value="0x05" name="German" />
    <entry value="0x07" name="Spanish" />
    <entry value="0x08" name="Korean" />
    <entry value="0x09" name="ChineseSimplified" />
    <entry value="0x0A" name="ChineseTraditional" />
  </enumDef>

  <enumDef name="PlayerInfo.SkinTint" type="uint8">
    <entry value="0"  name="MalePale" />       <!-- 0b000_000_00 -->
    <entry value="4"  name="FemalePale" />     <!-- 0b000_001_00 -->
    <entry value="8"  name="MaleDefault" />    <!-- 0b000_010_00 -->
    <entry value="12" name="FemaleDefault"  /> <!-- 0b000_011_00 -->
    <entry value="16" name="MaleTan"  />       <!-- 0b000_100_00 -->
    <entry value="20" name="FemaleTan"  />     <!-- 0b000_101_00 -->
    <entry value="24" name="MaleDark"  />      <!-- 0b000_110_00 -->
    <entry value="28" name="FemaleDark"  />    <!-- 0b000_111_00 -->
  </enumDef>

  <enumDef name="PlayerInfo.SpecialMoveUnlocks" type="uint8">
    <entry value="0" name="None" />
    <entry value="1" name="Mega Evolution" />
    <entry value="2" name="Z-Move" />
    <entry value="3" name="Both" />
  </enumDef>

  <enumDef name="PlayerInfo.BallThrowStyle" type="uint8">
    <entry value="0" name="Normal" />
    <entry value="1" name="Elegant" />
    <entry value="2" name="Girlish" />
    <entry value="3" name="Reverent" />
    <entry value="4" name="Smug" />
    <entry value="5" name="LeftHanded" />
    <entry value="6" name="Passionate" />
    <entry value="7" name="Idol" />
    <entry value="7" name="Nihilist" />

  </enumDef>

  <!-- Pc Box Layout -->

  <enumDef name="PcBoxBackground" type="uint8">
    <entry value="0x0" name="Forest" />
    <entry value="0x1" name="City" />
    <entry value="0x2" name="Desert" />
    <entry value="0x3" name="Savanna" />

    <entry value="0x4" name="Crag" />
    <entry value="0x5" name="Volcano" />
    <entry value="0x6" name="Snow" />
    <entry value="0x7" name="Case" />

    <entry value="0x8" name="Beach" />
    <entry value="0x9" name="Seafloor" />
    <entry value="0xA" name="River" />
    <entry value="0xB" name="Sky" />

    <entry value="0xC" name="PokeCenter" />
    <entry value="0xD" name="Machine" />
    <entry value="0xE" name="Checks" />
    <entry value="0xF" name="Simple" />
  </enumDef>

  <enumDef name="PcBoxFlags" type="uint8">
    <entry value="0" name="None" />

    <entry value="1" name="Unknown Flag 0" />
    <entry value="2" name="Unknown Flag 1" />
    <entry value="4" name="Unknown Flag 2" />
    <entry value="8" name="Unknown Flag 3" />
    <entry value="16" name="Unknown Flag 4" />
    <entry value="32" name="Unknown Flag 5" />
    <entry value="64" name="Unknown Flag 6" />
    <entry value="128" name="Unknown Flag 7" />
  </enumDef>
</data>
