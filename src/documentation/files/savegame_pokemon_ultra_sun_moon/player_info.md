# Player Info

<!-- |              Trainer Id |            0x00 |        0x1400 |    0x02 |             | uint16                           | Simple number. Does not reflect the real TID the game uses. That one can be calculated using the SID: `= (TID + (SID << 16)) % 1000000`. Or read both IDs together (4 bytes as uint32) and `%= 1000000` it | -->
<!-- |               Secret Id |            0x02 |        0x1402 |    0x02 |             | uint16                           |                                         | -->

|                    What | Relative Offset | Global Offset |    Size |      Length | Format                           | Description |
|-------------------------|----------------:|--------------:|--------:|------------:|----------------------------------|-----------------------------------------|
|             Trainer IDs |            0x00 |        0x1400 |    0x04 |             | uint32*                          | <p>To get the SID (Secret ID), take the first 4 digits of the decimal value.</p><p>To get the TID (Trainer ID), take the last 10 digits of the decimal value.</p><p>To get the pseudo TID/SID, read two bytes as a uint16 respectively <small>(in case this is meaningful in some way)</small>.</p> |
|                    Game |            0x04 |        0x1404 |    0x01 |             | [enum](#game-mapping)            | The save file contains information about what game it belongs to (only mainline Gen7: (ultra sun/moon)). See mapping below for values. |
|                  Gender |            0x05 |        0x1405 |    0x01 |             | [enum](#gender-mapping)          | See mapping below for values.           |
|                         |                 |               |         |             |                                  |                                         |
|              Sub-Region |            0x2E |        0x142E |    0x01 |             | [enum](#sub-region-mapping)          | The region within the country (state, county, etc.). Actual meaning depends on the country. See mapping below for values. |
|                 Country |            0x2F |        0x142E |    0x01 |             | [enum](#country-mapping)         | Country of the player. See mapping below for values. |
|                 Unknown |            0x30 |        0x1430 |    0x04 |             |                                  | Bytes have a value, whose purpose is not known :/ |
|          Console Region |            0x34 |        0x1434 |    0x01 |             | [enum](#console-region-mapping)  | Region of the 3DS. See mapping below for values. |
|                Language |            0x35 |        0x1435 |    0x01 |             | [enum](#language-mapping)        | See mapping below for values. |
|               ? Padding |            0x36 |        0x1436 |    0x02 |             |                                  | Bytes are all 0x00                      |
|             Player Name |            0x38 |        0x1438 |    0x18 |        0x0C | UTF-16 String                    | The name of the player. Encoded as fixed-size UTF-16. The end is padded with null bytes. |
|               ? Padding |            0x50 |        0x1450 |    0x04 |             |                                  | Bytes are all 0x00. Might belong to be following data as flags.                     |
|               Skin Tint |            0x54 |        0x1450 |    0x01*|             | [enum](#skin-tint-mapping)*      | Also contains other data. Only bits 00011100 are for the skin tint. The other bits are unknown. See mapping below for values. |
|               ? Padding |            0x55 |        0x1455 |    0x03 |             |                                  | Bytes are all 0x00. Might belong to be previous data as flags.                     |
| ? Multiplayer Sprite Id |            0x58 |        0x1458 |    0x01 |             |                                  | Unused in Gen7.                         |
|               ? Padding |            0x59 |        0x1459 |    0x01 |             |                                  | Byte is 0x00.                           |
|                 Unknown |            0x5A |        0x145A |    0x1E |             |                                  | Some bytes have a value, some don't. Their purpose is not known :/ |
|    Special Move Unlocks |            0x78 |        0x1478 |    0x01 |             | [flags](#special-move-unlocks)   | Whether mega evolutions and/or Z-moves are unlocked. See flags below for values.) |
|                 Unknown |            0x79 |        0x1479 |    0x01 |             |                                  | Byte is 0x00. Probably more flags.      |
|        Ball Throw Style |            0x7A |        0x147A |    0x01 |             | [enum](#ball-throw-type-mapping) | See mapping below for values.           |
|                         |                 |               |         |             |                                  |                                         |
| Padding until chunk end |            0x7B |        0x147B |   0x185 |             |                                  | No more data, just some padding to align the chunk. |
|                         |                 |               |         |             |                                  |                                         |
|                         |                 |               |         |             |                                  |                                         |

## Layout

{{#include player_info_block_layout.html}}

## Enum Mappings

### Game Mapping

```rs
#[repr(u8)]
enum Game {
    Sun       = 30,
    Moon      = 31,
    UltraSun  = 32,
    UltraMoon = 33,
}
```

### Gender Mapping

```rs
#[repr(u8)]
enum Gender {
    Male   = 0,
    Female = 1,
}
```

### Country Mapping

This list is **incomplete**!

```rs
#[repr(u8)]
enum Country {
    Albania  = 0x40,
    Germany  = 0x4E,
    Norway   = 0x60,
    Zimbabwe = 0x70,
    Andorra  = 0x7A,
}
```

### Sub Region Mapping

A value of `0x00` means "N/A".
`0x01` seems to be reserved for countries that have no sub-regions.

This list is **incomplete**!

<details>
    <summary>Click to expand</summary>

#### Germany

```rs
#[repr(u8)]
enum SubRegionGermany {
    Berlin                   = 0x02,
    Hesse                    = 0x03,
    BadenWurttemberg         = 0x04,
    Bavaria                  = 0x05,
    Brandenburg              = 0x06,
    Bremen                   = 0x07,
    Hamburg                  = 0x08,
    MecklenburgWestPomerania = 0x09,
    LowerSaxony              = 0x0A,
    NorthRhineWestphalia     = 0x0B,
    RhinelandPalatinate      = 0x0C,
    Saarland                 = 0x0D,
    Saxony                   = 0x0E,
    SaxonyAnhalt             = 0x0F,
    SchleswigHolstein        = 0x10,
    Thuringia                = 0x11,
}
```

</details>

### Console Region Mapping

```rs
#[repr(u8)]
enum ConsoleRegion {
    Japan     = 0x00,
    America   = 0x01, // North and South America
    Europe    = 0x02,  // Europe and Australia
    // Unused = 0x03, // Maybe Australia was planned to be its own region at some point?
    China     = 0x04,
    Korea     = 0x05,
    Taiwan    = 0x06,
}
```

### Language Mapping

```rs
#[repr(u8)]
enum Language {
    // None            = 0x00, // Invalid // Not sure why JPN starts at 0x01
    Japanese           = 0x01,
    English            = 0x02,
    French             = 0x03,
    Italian            = 0x04,
    German             = 0x05,
    // Unused          = 0x06,
    Spanish            = 0x07,
    Korean             = 0x08,
    ChineseSimplified  = 0x09,
    ChineseTraditional = 0x0A,
}
```

### Skin Tint Mapping

This mapping assumes to be right-aligned (the byte contains more info than only skin tint).\
<small><small>(`value = raw_byte >> 2`)</small></small>

```rs
enum SkinTint {
    MalePale      = 0x00,
    FemalePale    = 0x01,
    MaleDefault   = 0x02,
    FemaleDefault = 0x03,
    MaleTan       = 0x04,
    FemaleTan     = 0x05,
    MaleDark      = 0x06,
    FemaleDark    = 0x07,
}
```
### Ball Throw Type Mapping

```rs
#[repr(u8)]
enum BallThrowStyle {
    Normal     = 0x00,
    Elegant    = 0x01,
    Girlish    = 0x02,
    Reverent   = 0x03,
    Smug       = 0x04,
    LeftHanded = 0x05,
    Passionate = 0x06,
    Idol       = 0x07,
    Nihilist   = 0x08,
}
```

## Flags

### Special Move Unlocks

```rs
struct SpecialMoveUnlocks: u8 {
    const MegaEvolution = 0b0000_0001;
    const ZMove         = 0b0000_0010;

    const None = 0b0000_0000;
    const Both = 0b0000_0011;
}
```

## References

- Trainer/Secret ID: <https://gamefaqs.gamespot.com/boards/210930-pokemon-ultra-sun/77179422#:~:text=%239>
