# Random Stuff

|                What | Global Offset |    Size |      Length |               Format | Description |
|---------------------|--------------:|--------:|------------:|----------------------|-----------------------------------------|
|               Money |        0x4404 |    0x04 |             |               uint32 | Simple number indicating how many boxes have been unlocked. In-game max is 9_999_999. |
|                     |               |         |             |                      |                                         |
|                     |               |         |             |                      |                                         |
|                     |               |         |             |                      |                                         |
