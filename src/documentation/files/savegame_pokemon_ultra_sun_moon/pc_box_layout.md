# PC / Box Layout

|                What | Relative Offset | Global Offset |    Size |      Length |               Format | Description |
|---------------------|----------------:|--------------:|--------:|------------:|----------------------|-----------------------------------------|
|           Box Names |          0x0000 |        0x4C00 |   0x440 |      0x220* |       UTF-16 Strings | The names of the PC boxes. Those are 32 strings, each being 17 chars / 34 bytes long, encoded as fixed-size UTF-16. The end is padded with null bytes |
|          Team Names |          0x0440 |        0x5040 |    0x84 |       0x42* |       UTF-16 Strings | The names of the battle teams. Those are 6 strings, each being 11 chars / 22 bytes long, encoded as fixed-size UTF-16. The end is padded with null bytes |
|           _Unknown_ |          0x04C4 |        0x50C4 |    0x48 |             |                      | 72 bytes that are all FF                |
|           _Unknown_ |          0x050C |        0x510C |    0xB4 |             |                      | 180 bytes that are all 00               |
|     Box Backgrounds |          0x05C0 |        0x51C0 |    0x20 |             |                 enum | The backgrounds of each PC box. Each box has 1 byte here, ranging from 0x00 to 0x0F for 16 different backgrounds. See the list below for the mapping. |
|               Flags |          0x05E0 |        0x51E0 |    0x01 |             |                flags | ?                                       |
|      Unlocked Count |          0x05E1 |        0x51E1 |    0x01 |             |                uint8 | Simple number indicating how many boxes have been unlocked. |
|                     |                 |               |         |             |                      |                                         |
|                     |                 |               |         |             |                      |                                         |
|                     |                 |               |         |             |                      |                                         |

## Mappings

### Box Background Mapping

```rs
#[repr(u8)]
enum Background {
    Forest = 0,
    City = 1,
    Desert = 2,
    Savanna = 3,

    Crag = 4,
    Volcano = 5,
    Snow = 6,
    Case = 7,

    Beach = 8,
    Seafloor = 9,
    River = 10,
    Sky = 11,

    PokeCenter = 12,
    Machine = 13,
    Checks = 14,
    Simple = 15,
}
```
