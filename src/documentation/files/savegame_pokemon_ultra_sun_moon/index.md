# Savegame Pokémon Ultra Sun / Ultra Moon

As far as I know the file size is fixed and its contents are at fixed offsets, making it easy to document and work with.
The file size for USUM save files is 445440 / 0x6CC00 bytes.
That's an additional 3584 / 0xE00 bytes more compared to regular SM.

The savegame is split into 870 chunks, each being 0x200 / 515 bytes big.
Thus, each chunk is aligned to start on a nice 0x200 offset.
For example one chunk starts at 0x0400 and is 42 bytes long, then the next chunk starts at 0x600.

The following table contains relevant chunks with their offsets.
Global offset means from the beginning of the file.
The relative offset is relative to the thing each table describes.
Size means *bytes*, Length means *chars*.
Chunks is the number of 0x200 sized chunks a section uses.

Numbers should all be in little endian.

Entries with an asterisk (`*`) have additional context.
Entries with a question mark (`?`) might not be correct / unsure.
Comparison signs (`<`, `<=`, `>`, `>=`) give more context when the value is unsure.

|                                            What | Global Offset |        Size | Chunks | Block                                 |
|-------------------------------------------------|--------------:|------------:|-------:|:-------------------------------------:|
|                 [Player Info](./player_info.md) |        0x1400 |  ? <= 0x200 |      1 | <div class="block yellow"></div>      |
|           [PC / Box Layout](./pc_box_layout.md) |        0x4C00 |  ? >= 0x5E2 |      3 | <div class="block pale-red"></div>    |
|                                                 |               |             |      1 |                                       |
|               [Random Stuff](./random_stuff.md) |               |             |      n |                                       |

## Block Layout

{{#include index_block_layout.html}}

## References

-
