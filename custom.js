const addImageTitles = () => {
    // Selects all images with a non-empty alt attribute that also don't have a title
    const imagesWithAlt = document.querySelectorAll('img[alt]:not(img[alt=""]):not(img[title])');
    for (const img of imagesWithAlt) {
        img.setAttribute('title', img.getAttribute('alt'))
    }
}

addImageTitles();
